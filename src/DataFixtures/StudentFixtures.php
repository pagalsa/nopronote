<?php

namespace App\DataFixtures;

use App\Entity\Discipline;
use App\Entity\Student;
use App\Entity\User;
use Doctrine\Common\Collections\Collection;
use Faker\Factory;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class StudentFixtures extends AppFixtures
{
    private $faker;
    private $passwordEncoder;
    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }
    public function getDependencies()
    {
        return [DisciplineFixtures::class];
    }

    public function loadData(ObjectManager $manager)
    {
        $this->faker  = Factory::create();
        $this->createMany(Student::class, 300,
            function(Student $student,$i){
                $user = new User();
                $user
                    ->setEmail($this->faker->freeEmail)
                    ->setRoles(["ROLE_STUDENT"])
                    ->setPassword($this->passwordEncoder->encodePassword(
                        $user,"azertyuiop"
                    ));
                $disciplineArray = [
                    $this->getReference(Discipline::class . '_0'),
                    $this->getReference(Discipline::class .'_1'),
                    $this->getReference(Discipline::class . '_2'),
                    $this->getReference(Discipline::class . '_3'),
                    $this->getReference(Discipline::class . '_4')];
                $student
                    ->setLastName($this->faker->lastName)
                    ->setFirstName($this->faker->firstName($gender = null))
                    ->setUser($user);
                    //->setDiscipline($disciplineArray);
                    if($i <100){
                        $student->setLevel("Seconde");
                    }elseif ($i>=100 && $i<200){
                        $student->setLevel("Première");
                    }else{
                        $student->setLevel("Terminal");
                    }
            });
        $manager->flush();
    }
}
