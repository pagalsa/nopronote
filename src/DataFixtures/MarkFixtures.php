<?php

namespace App\DataFixtures;

use App\Entity\Mark;
use App\Entity\Student;
use App\Entity\Teacher;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class MarkFixtures extends AppFixtures implements DependentFixtureInterface
{
    private $faker;

    public function getDependencies()
    {
        return [TeacherFixtures::class,StudentFixtures::class];
    }
    public function loadData(ObjectManager $manager)
    {
        $this->faker = Factory::create();
        $this->createMany(Mark::class, 1000,
            function (Mark $mark) {
                $teacher = $this->getReference(Teacher::class . '_' .rand(0, 14));
                $mark
                    ->setComment($this->faker->paragraph($nbSentences = 3, $variableNbSentences = true))
                    ->setMark(rand(0, 20))
                    ->setValuationDay(new \DateTime())
                    ->setValuationDaySend(new \DateTime())
                    ->setStudent($this->getReference(Student::class . '_' .rand(0, 299)))
                    ->setTeacher($teacher)
                    ->setDiscipline($teacher->getDiscipline());

            });
        $manager->flush();
    }
}
