<?php

namespace App\DataFixtures;

use App\Entity\Discipline;
use App\Entity\Mark;
use App\Entity\Student;
use Doctrine\Persistence\ObjectManager;

class DisciplineFixtures extends AppFixtures
{
    public function getDependencies()
    {
        return [StudentFixtures::class];
    }
    public function loadData(ObjectManager $manager)
    {
        $this->createMany(Discipline::class, 5,
            function(Discipline $discipline,$i){
                $disciplinesTab = ["Français","Anglais","Mathématique","Physique-Chimie","Education-Sportive"];
                $discipline
                    ->setName($disciplinesTab[$i]);
        });
        $manager->flush();
    }
}
