<?php

namespace App\DataFixtures;

use App\Entity\Discipline;
use App\Entity\Teacher;
use App\Entity\User;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class TeacherFixtures extends AppFixtures implements DependentFixtureInterface
{
    private $faker;
    private $y;
    private $passwordEncoder;
    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder= $passwordEncoder;
    }

    public function getDependencies()
    {
        return [DisciplineFixtures::class];
    }

    public function loadData(ObjectManager $manager)
    {
        $this->y = 0;
        $this->faker = Factory::create();
        $this->createMany(Teacher::class, 15,
            function (Teacher $teacher) {
                $user = new User();
                $user
                    ->setEmail($this->faker->freeEmail)
                    ->setRoles(["ROLE_TEACHER"])
                    ->setPassword($this->passwordEncoder->encodePassword(
                        $user,"azertyuiop"
                    ));
                $teacher
                    ->setLastName($this->faker->lastName)
                    ->setFirstName($this->faker->firstName($gender = null))
                    ->setUser($user);

                if ($this->y <=4){
                    $teacher->setDiscipline($this->getReference(Discipline::class . '_' . $this->y));
                    $this->y++;
                }else{
                    $this->y=0;
                    $teacher->setDiscipline($this->getReference(Discipline::class . '_' . $this->y));
                    $this->y++;
                }
            });
        $manager->flush();
    }
}
