<?php

namespace App\Controller;

use App\Form\RegistrationFormType;
use App\Form\SearchStudentType;
use App\Repository\StudentRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SearchController extends AbstractController
{
    /**
     * @Route("/search", name="search")
     * @param Request $request
     * @param StudentRepository $studentRepository
     * @return Response
     */
    public function index(Request $request, StudentRepository $studentRepository): Response
    {
        $SearchStudent = $this->createForm(SearchStudentType::class);
        $SearchStudent->handleRequest($request);
        $StudentCorrespondList = null;
        $students = $studentRepository->findAll();
        if ($SearchStudent->isSubmitted() && $SearchStudent->isValid()) {
            $level = $SearchStudent->get('level')->getData();
            $name = $SearchStudent->get('lastName')->getData();

            $StudentCorrespondList = $studentRepository->findStudentByNameAndLevel($name,$level);

            return $this->render('search/index.html.twig', [
                'StudentCorrespondList' => $StudentCorrespondList,
                'searchStudent' => $SearchStudent->createView(),
                "AllStudents"=> $students,
            ]);
        }


        return $this->render('search/index.html.twig', [
            'StudentCorrespondList' => $StudentCorrespondList,
            'searchStudent' => $SearchStudent->createView(),
            "AllStudents"=> $students,
        ]);
    }
}
