<?php

namespace App\Controller;

use App\Entity\Mark;
use App\Form\AddMarkType;
use App\Repository\StudentRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TeacherViewStudentController extends AbstractController
{
    /**
     * @Route("student/{id}", name="teacher_view_student", requirements={"id"="\d+"})
     * @param int $id
     * @param StudentRepository $studentRepository
     * @param Request $request
     * @return Response
     */
    public function index(int $id, StudentRepository $studentRepository, Request $request): Response
    {
        $student = $studentRepository->findOneBy(["id"=>$id]);
        $allMarksFiltered = $this->organiseMark($student);

        $addMark = $this->createForm(AddMarkType::class);
        $addMark->handleRequest($request);

        if ($addMark->isSubmitted() && $addMark->isValid()) {
            $mark = new Mark();
            $mark
                ->setDiscipline($this->getUser()->getTeacher()->getDiscipline())
                ->setStudent($student)
                ->setValuationDaySend(new \DateTime)
                ->setValuationDay($addMark->get("valuationDay")->getData())
                ->setTeacher($this->getUser()->getTeacher())
                ->setMark($addMark->get("mark")->getData())
                ->setComment($addMark->get("comment")->getData());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($mark);
            $entityManager->flush();

            return $this->render('teacher_view_student/valideAddMark.html.twig', []);
        }

        return $this->render('teacher_view_student/index.html.twig', [
            "studentObj"=> $student,"allMarks"=>$allMarksFiltered,'addMarkForm' => $addMark->createView()
        ]);
    }
    public function organiseMark($student){

        $allMarks = $student->getMarks();

        $math = [];
        $french = [];
        $physics = [];
        $english = [];
        $sports = [];

        for ($i = 0; $i< count($allMarks); $i++){
            if ($allMarks[$i]->getDiscipline()->getName()=="Mathématique"){
                array_push($math, $allMarks[$i]->getMark());
            }elseif ($allMarks[$i]->getDiscipline()->getName()=="Français"){
                array_push($french, $allMarks[$i]->getMark());
            }elseif ($allMarks[$i]->getDiscipline()->getName()=="Physique-Chimie"){
                array_push($physics, $allMarks[$i]->getMark());
            }elseif ($allMarks[$i]->getDiscipline()->getName()=="Anglais"){
                array_push($english, $allMarks[$i]->getMark());
            }elseif ($allMarks[$i]->getDiscipline()->getName()=="Education-Sportive"){
                array_push($sports, $allMarks[$i]->getMark());
            }
        }
        return [
            "Mathématique"=>$math,
            "Français"=>$french,
            "Physique-Chimie"=>$physics,
            "Anglais"=>$english,
            "Education-Sportive"=>$sports
        ];
    }
}
