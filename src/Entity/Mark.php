<?php

namespace App\Entity;

use App\Repository\MarkRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MarkRepository::class)
 */
class Mark
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Student::class,inversedBy="marks", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $student;

    /**
     * @ORM\ManyToOne(targetEntity=Teacher::class, cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $teacher;

    /**
     * @ORM\Column(type="decimal", precision=4, scale=2)
     */
    private $mark;

    /**
     * @ORM\Column(type="text", length=255, nullable=true)
     */
    private $comment;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $valuationDay;

    /**
     * @ORM\Column(type="datetime")
     */
    private $valuationDaySend;

    /**
     * @ORM\ManyToOne (targetEntity=Discipline::class, inversedBy="mark", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $discipline;

     public function getId(): ?int
    {
        return $this->id;
    }

    public function getStudent(): ?Student
    {
        return $this->student;
    }

    public function setStudent(Student $student): self
    {
        $this->student = $student;

        return $this;
    }

    public function getTeacher(): ?Teacher
    {
        return $this->teacher;
    }

    public function setTeacher(Teacher $teacher): self
    {
        $this->teacher = $teacher;

        return $this;
    }

    public function getMark(): ?string
    {
        return $this->mark;
    }

    public function setMark(string $mark): self
    {
        $this->mark = $mark;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getValuationDay(): ?\DateTime
    {
        return $this->valuationDay;
    }

    public function setValuationDay(?\DateTimeInterface $valuationDay): self
    {
        $this->valuationDay = $valuationDay;

        return $this;
    }

    public function getValuationDaySend(): ?\DateTime
    {
        return $this->valuationDaySend;
    }

    public function setValuationDaySend(\DateTimeInterface $valuationDaySend): self
    {
        $this->valuationDaySend = $valuationDaySend;

        return $this;
    }

    public function getDiscipline(): ?Discipline
    {
        return $this->discipline;
    }

    public function setDiscipline(Discipline $discipline): self
    {
        $this->discipline = $discipline;

        return $this;
    }
}
